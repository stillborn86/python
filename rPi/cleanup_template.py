#!/usr/bin/python

"""
2022XXxx - Created by stillborn86 (c)

This sketch is designed...

"""

import signal
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

class gracefulKiller:
    killNow = False

    def __init__( self ):
        signal.signal( signal.SIGINT, self.exitGracefully )
        signal.signal( signal.SIGTERM, self.exitGracefully )

    def exitGracefully( self, *args ):
        self.killNow = True

try:
    if __name__ == '__main__':
        killer = gracefulKiller()

        while not killer.killNow:


except KeyboardInterrupt:
    print( "\nA keyboard interrupt has been detected!" )

except:
    print( "\nAn error or exception has occurred!" )

finally:
    GPIO.cleanup()
    print( "\nProgram exited successfully." )
    quit()

