import random

stuffs = ['nothing', 'Rock', 'Paper', 'Scissors']
play = True

while play:
    print("Please enter your choice...\n\t1. Rock\n\t2. Paper\n\t3. Scissors")
    pC = int(input("\t?: "))

    while (pC < 1) or (pC > 3):
        print("You have made a bad choice, and you should feel bad... \n\n")
        print("Please enter your choice...\n\t1. Rock\n\t2. Paper\n\t3. Scissors")
        pC = int(input("\t?: "))

    cC = random.randint(1, 3)

    if pC == cC:
        print("\nTie, since you both chose %s" % stuffs[pC])
    elif (pC == 1 and cC == 3) or pC > cC:
        print("\nYou win, with %s, since the computer chose %s!" % (stuffs[pC], stuffs[cC]))
    else:
        print("\nThe computer won with %s, since you chonse %s." % (stuffs[cC], stuffs[pC]))

    print("\nDo you want to play again?\n\t1. Yes\n\t2. No")
    choice = int(input("\t?: "))

    play = choice%2

