#!/usr/bin/env python

import random

faces = [ 0, 0, 0, 0, 0 ,0, 0 ]
rolls = 0

def getStats():
    print("\nIn %d rolls, this is your statistical breakdown:\n" % rolls)
    
    for i in range(1,7):
        print("\nThe #%d side showed up %d times." % (i, faces[i]))
        print("And this side showed up %.2f%% of the time\n" % (100*float(faces[i]/rolls))) 

try:
    while True:
        face = random.randint(1,6)
        rolls += 1
        print("You rolled a %d on the %d roll." % (face, rolls))

        faces[face] += 1

except KeyboardInterrupt:
    print("\nA keyboard interrupt has been detected!")
    
    getStats()

except:
    print("\nAn error or exception has occurred!")

