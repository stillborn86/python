import random

l1 = random.randint(5, 15)
l2 = random.randint(5, 15)

a = []
b = []
combo = []

for i in range(0, l1):
    a.append(random.randint(0, 65))

for i in range(0, l2):
    b.append(random.randint(0, 65))

for i in range(0, 65):
    if (i in a) and (i in b):
        combo.append(i)

print("List A is: %r" % a)
print("List B is: %r" % b)

print("\nCommon elements are: %r" % combo)

