import random

length = random.randint(0, 15)

a = []
newList = []

for i in range(0, length):
    a.append(random.randint(0, 35))

print("Your random list is: %r" % a)

for i in a:
    if not (i%2):
        newList.append(i)

print("\nThe even elements of your list are: %r" % newList)

