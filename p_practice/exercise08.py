import random

play = True

while play:
    print("Please enter your choice...\n\t1. Rock\n\t2. Paper\n\t3. Scissors")
    playerChoice = int(input("\t?: "))

    while (playerChoice < 1) or (playerChoice > 3):
        print("You have made a bad choice, and you should feel bad... \n\n")
        print("Please enter your choice...\n\t1. Rock\n\t2. Paper\n\t3. Scissors")
        playerChoice = int(input("\t?: "))

    compChoice = random.randint(1, 3)

    if playerChoice == 1:
        pC = "Rock"
    elif playerChoice == 2:
        pC = "Paper"
    elif playerChoice == 3:
        pC = "Scissors"

    if compChoice == 1:
        cC = "Rock"
    elif compChoice == 2:
        cC = "Paper"
    elif compChoice == 3:
        cC =  "Scissors"

    if playerChoice == compChoice:
        print("\nTie, since you both chose %s" % pC)
    elif (playerChoice == 1 and compChoice == 3) or playerChoice > compChoice:
        print("\nYou win, with %s, since the computer chose %s!" % (pC, cC))
    else:
        print("\nThe computer won with %s, since you chonse %s." % (cC, pC))

    print("\nDo you want to play again?\n\t1. Yes\n\t2. No")
    choice = int(input("\t?: "))

    play = choice%2

