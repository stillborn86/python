import random

num = random.randint(1, 9)

while True:
    select = input("Please enter a guess (1-9): ")

    if select == 'exit':
        break
    else:
        guess = int(select)
    
    if guess < num:
        print("Your guess is too low.")
    elif guess > num:
        print("Your guess is too high.")
    else:
        print("You guessed correctly!")
        break

